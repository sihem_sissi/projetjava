package array;

import java.util.Arrays;

public class TriavecSort {

   public static void main(String[] args) {

   // initialiser le tableau
   int array[] = {9,-10,0, 34, -23, 60, 6,1000, 41,0,11};

   // afficher tous les entiers avant le tri
   for (int entier : array) {
      System.out.println("nombre: " + entier);
   }

   // trier le tableau
   Arrays.sort(array);

   // lafficher tous les entiers apr�s le tri
   System.out.println("Tableau tri�\n");
   for (int entier : array) {
      System.out.println("nombre: " + entier);
   }
   }
}
package tpSwichIfFor;
import java.util.Scanner;
public class Damiencorrectioncalculatrice {

	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);
		int nb1, nb2, resultat;
		int choix;
		do {
			System.out.println("\n\n\n\t\tMENU PRINCIPAL\n");
			System.out.println("\t1. Additionner deux nombres\n");
			System.out.println("\t2. Soustraire deux nombres\n");
			System.out.println("\t3. Multiplier deux nombres\n");
			System.out.println("\t4. Diviser deux nombres\n");
			System.out.println("\t0. Quitter\n");
			System.out.print("\tFaites votre choix : ");
			choix = clavier.nextInt();
			switch (choix) {
			case 1:
				System.out.print("\n\tEntrez le nombre a : ");
				nb1 = clavier.nextInt();				
				System.out.print("\n\tEntrez le nombre b : ");
				nb2 = clavier.nextInt();
				resultat = nb1 + nb2;
				System.out.println("\n\t" + nb1 + " + " + nb2 + " = " + resultat);
				break;
			case 2:
				System.out.print("\n\tEntrez le nombre a : ");
				nb1 = clavier.nextInt();
				System.out.print("\n\tEntrez le nombre b : ");
				nb2 = clavier.nextInt();
				resultat = nb1 - nb2;
				System.out.println("\n\t" + nb1 + " + " + nb2 + " = " + resultat);
				break;
			case 3:
				System.out.print("\n\tEntrez le nombre a : ");
				nb1 = clavier.nextInt();
				System.out.print("\n\tEntrez le nombre b : ");
				nb2 = clavier.nextInt();
				resultat = nb1 * nb2;
				System.out.println("\n\t" + nb1 + " + " + nb2 + " = " + resultat);
				break;
			case 4:
				System.out.print("\n\tEntrez le nombre a : ");
				nb1 = clavier.nextInt();
				System.out.print("\n\tEntrez le nombre b : ");
				nb2 = clavier.nextInt();
				if (nb2 != 0) {
					resultat = nb1 / nb2;
					System.out.println("\n\t" + nb1 + " + " + nb2 + " = " + resultat);
				} else
					System.out.println("\n\t le nombre 2 est nul, division par 0 est impossible ");
				break;
			}
		} while (choix != 0);
		// TODO Auto-generated method stub

	}

}
